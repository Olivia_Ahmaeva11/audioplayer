const resultBlock = document.getElementById("results");
const serchBtn = document.getElementById("searchBtn");
const serchInp = document.getElementById("serchInp") as HTMLInputElement;
const musPlayer = document.getElementById("player");
const nextTrackBtn = document.getElementById("next");
const prevTrackBtn = document.getElementById("prev");
const pausePlayBtn = document.getElementById("playBtn");

const artLib: object = {
  0: {
    artistName: "Bob",
    artistAlbs: "RoseBan 12.03.2999, SecAlb 12.12.12",
    artistTracs: "Sky 3:04, Circle of Salt 2:23, Saldom 1:05, ftMike 5:22",
  },
  1: {
    artistName: "The Beatles",
    artistAlbs: "Youth and society 12.03.1969",
    artistTracs: "HellBoy 3:04 badTrack, Marry 2:23, Yellow Submarine 1:05",
  },
  2: {
    artistName: "AC/DC",
    artistAlbs: "Good Day 15.03.2002",
    artistTracs: "Road 3:04, Back in Black 2:23, Be main 1:05 badTrack",
  },
  3: {
    artistName: "Mike",
    artistAlbs: "MikeFlexSuper 19.03.2020",
    artistTracs:
      "Flowers 3:04, Happy 2:23, Railway Station 1:05, ftBob 5:22",
  },
};

class Singleton {
  private static instance: Singleton;

  private _arrRes: Array<object | string> = [];

  private _searchValue: string = "";

  private _tracksArr: Array<string> = [];

  private _playPause: boolean = false;

  private _trackIndex: number = 0;

  private constructor() {}

  public static getInstance(): Singleton {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton();
    }

    return Singleton.instance;
  }

  private _setTracks(track: string): void {
    this._tracksArr.push(track);
    this._setMusPlayerTrack(this._trackIndex);
  }

  private _setMusPlayerTrack(number: number): void {
    musPlayer.innerHTML = this._tracksArr[number];
  }

  public nextTrack(): void {
    if (this._trackIndex + 1 > this._tracksArr.length - 1) {
      this._trackIndex = 0;
      this._setMusPlayerTrack(this._trackIndex);
    } else {
      this._trackIndex += 1;
      this._setMusPlayerTrack(this._trackIndex);
    }
  }

  public prevTrack(): void {
    if (this._trackIndex - 1 < 0) {
      this._trackIndex = this._tracksArr.length - 1;
      this._setMusPlayerTrack(this._trackIndex);
    } else {
      this._trackIndex -= 1;
      this._setMusPlayerTrack(this._trackIndex);
    }
  }

  public setResult<T>(arrayProp: Array<T>): void {
    if (arrayProp.length == 0 || arrayProp == null) {
      resultBlock.innerHTML = "nothing found";
    } else {
      for (let i = 0; i < arrayProp.length; i++) {
        let inHtResBl: string = "";
        let cardElement = document.createElement("div");
        cardElement.classList.add("container__card");

        for (let key in arrayProp[i]) {
          if (key == "artistName") {
            inHtResBl += `<h1 class="artist-title">Artist name: ${arrayProp[i][key]}</h1>`;
          } else if (key == "artistAlbs") {
            let strAlb: Array<string> = String(arrayProp[i][key]).split(",");
            inHtResBl += `<div><h2 class="artist-title">Albums:</h2></div>`;

            for (let j = 0; j < strAlb.length; j++) {
              inHtResBl += `<div><p>${strAlb[j]}</p></div>`;
            }
          } else if (key == "artistTracs") {
            let strTracks: Array<string> = String(arrayProp[i][key]).split(",");
            inHtResBl += `<div><h2 class="artist-title">Tracks:</h2></div>`;

            for (let f = 0; f < strTracks.length; f++) {
              this._setTracks(String(strTracks[f]));

              let findBad = strTracks[f];
              if (findBad.slice(-8) == "badTrack") {
                let badTrackEf = '<span class="container__track_bad">b#d<span>';
                strTracks[f] = strTracks[f].slice(0, -8);
                inHtResBl += `<div>${strTracks[f]}  ${badTrackEf}</div>`;
              } else {
                inHtResBl += `<div>${strTracks[f]}</div>`;
              }
            }
          }
        }
        cardElement.innerHTML = inHtResBl;

        resultBlock.appendChild(cardElement);
      }
      this._arrRes = [];
    }
  }

  private search(library: object) {
    const date: object = library;

    for (let key in date) {
      let valueKey = date[key];

      if (typeof valueKey == "object") {
        for (let propName in valueKey) {
          if (
            valueKey[propName].toLocaleLowerCase().includes(this._searchValue)
          ) {
            this._arrRes.push(date[key]);
            break;
          }
        }
      }
    }
    this.setResult(this._arrRes);
  }

  public setSearch(toSearch: string): void {
    this._searchValue = toSearch.toLowerCase();

    this.search(artLib);
  }

  public _pausePlay(): void {
    if (this._playPause == false) {
      this._playPause = true;
      pausePlayBtn.innerHTML = '<i class="fas fa-pause-circle"></i>';
      alert("play");
    } else if (this._playPause == true) {
      this._playPause = false;
      pausePlayBtn.innerHTML = '<i class="fas fa-caret-right"></i>';
      alert("pause");
    }
  }
}

const s2 = Singleton.getInstance();

serchBtn.addEventListener("click", () => {
  resultBlock.innerHTML = "";

  const searchStr: string = serchInp.value;
  s2.setSearch(searchStr);
});

nextTrackBtn.addEventListener("click", () => {
  s2.nextTrack();
  // alert('next');
});

prevTrackBtn.addEventListener("click", () => {
  s2.prevTrack();
  // alert('prev');
});

pausePlayBtn.addEventListener("click", () => {
  s2._pausePlay();
});
