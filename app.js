var resultBlock = document.getElementById("results");
var serchBtn = document.getElementById("searchBtn");
var serchInp = document.getElementById("serchInp");
var musPlayer = document.getElementById("player");
var nextTrackBtn = document.getElementById("next");
var prevTrackBtn = document.getElementById("prev");
var pausePlayBtn = document.getElementById("playBtn");
var artLib = {
    0: {
        artistName: "Bob",
        artistAlbs: "RoseBan 12.03.2999, SecAlb 12.12.12",
        artistTracs: "Sky 3:04, Circle of Salt 2:23, Saldom 1:05, ftMike 5:22",
    },
    1: {
        artistName: "The Beatles",
        artistAlbs: "Youth and society 12.03.1969",
        artistTracs: "HellBoy 3:04 badTrack, Marry 2:23, Yellow Submarine 1:05",
    },
    2: {
        artistName: "AC/DC",
        artistAlbs: "Good Day 15.03.2002",
        artistTracs: "Road 3:04, Back in Black 2:23, Be main 1:05 badTrack",
    },
    3: {
        artistName: "Mike",
        artistAlbs: "MikeFlexSuper 19.03.2020",
        artistTracs: "Flowers 3:04, Happy 2:23, Railway Station 1:05, ftBob 5:22",
    },
};
var Singleton = /** @class */ (function () {
    function Singleton() {
        this._arrRes = [];
        this._searchValue = "";
        this._tracksArr = [];
        this._playPause = false;
        this._trackIndex = 0;
    }
    Singleton.getInstance = function () {
        if (!Singleton.instance) {
            Singleton.instance = new Singleton();
        }
        return Singleton.instance;
    };
    Singleton.prototype._setTracks = function (track) {
        this._tracksArr.push(track);
        this._setMusPlayerTrack(this._trackIndex);
    };
    Singleton.prototype._setMusPlayerTrack = function (number) {
        musPlayer.innerHTML = this._tracksArr[number];
    };
    Singleton.prototype.nextTrack = function () {
        if (this._trackIndex + 1 > this._tracksArr.length - 1) {
            this._trackIndex = 0;
            this._setMusPlayerTrack(this._trackIndex);
        }
        else {
            this._trackIndex += 1;
            this._setMusPlayerTrack(this._trackIndex);
        }
    };
    Singleton.prototype.prevTrack = function () {
        if (this._trackIndex - 1 < 0) {
            this._trackIndex = this._tracksArr.length - 1;
            this._setMusPlayerTrack(this._trackIndex);
        }
        else {
            this._trackIndex -= 1;
            this._setMusPlayerTrack(this._trackIndex);
        }
    };
    Singleton.prototype.setResult = function (arrayProp) {
        if (arrayProp.length == 0 || arrayProp == null) {
            resultBlock.innerHTML = "nothing found";
        }
        else {
            for (var i = 0; i < arrayProp.length; i++) {
                var inHtResBl = "";
                var cardElement = document.createElement("div");
                cardElement.classList.add("container__card");
                for (var key in arrayProp[i]) {
                    if (key == "artistName") {
                        inHtResBl += "<h1 class=\"artist-title\">Artist name: " + arrayProp[i][key] + "</h1>";
                    }
                    else if (key == "artistAlbs") {
                        var strAlb = String(arrayProp[i][key]).split(",");
                        inHtResBl += "<div><h2 class=\"artist-title\">Albums:</h2></div>";
                        for (var j = 0; j < strAlb.length; j++) {
                            inHtResBl += "<div><p>" + strAlb[j] + "</p></div>";
                        }
                    }
                    else if (key == "artistTracs") {
                        var strTracks = String(arrayProp[i][key]).split(",");
                        inHtResBl += "<div><h2 class=\"artist-title\">Tracks:</h2></div>";
                        for (var f = 0; f < strTracks.length; f++) {
                            this._setTracks(String(strTracks[f]));
                            var findBad = strTracks[f];
                            if (findBad.slice(-8) == "badTrack") {
                                var badTrackEf = '<span class="container__track_bad">b#d<span>';
                                strTracks[f] = strTracks[f].slice(0, -8);
                                inHtResBl += "<div>" + strTracks[f] + "  " + badTrackEf + "</div>";
                            }
                            else {
                                inHtResBl += "<div>" + strTracks[f] + "</div>";
                            }
                        }
                    }
                }
                cardElement.innerHTML = inHtResBl;
                resultBlock.appendChild(cardElement);
            }
            this._arrRes = [];
        }
    };
    Singleton.prototype.search = function (library) {
        var date = library;
        for (var key in date) {
            var valueKey = date[key];
            if (typeof valueKey == "object") {
                for (var propName in valueKey) {
                    if (valueKey[propName].toLocaleLowerCase().includes(this._searchValue)) {
                        this._arrRes.push(date[key]);
                        break;
                    }
                }
            }
        }
        this.setResult(this._arrRes);
    };
    Singleton.prototype.setSearch = function (toSearch) {
        this._searchValue = toSearch.toLowerCase();
        this.search(artLib);
    };
    Singleton.prototype._pausePlay = function () {
        if (this._playPause == false) {
            this._playPause = true;
            pausePlayBtn.innerHTML = '<i class="fas fa-pause-circle"></i>';
            alert("play");
        }
        else if (this._playPause == true) {
            this._playPause = false;
            pausePlayBtn.innerHTML = '<i class="fas fa-caret-right"></i>';
            alert("pause");
        }
    };
    return Singleton;
}());
var s2 = Singleton.getInstance();
serchBtn.addEventListener("click", function () {
    resultBlock.innerHTML = "";
    var searchStr = serchInp.value;
    s2.setSearch(searchStr);
});
nextTrackBtn.addEventListener("click", function () {
    s2.nextTrack();
    // alert('next');
});
prevTrackBtn.addEventListener("click", function () {
    s2.prevTrack();
    // alert('prev');
});
pausePlayBtn.addEventListener("click", function () {
    s2._pausePlay();
});
